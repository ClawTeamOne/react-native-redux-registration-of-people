/**
 * @format
 */

import React from 'react'
import {AppRegistry} from 'react-native';

import { Provider } from 'react-redux'
import configureStore from './src/store'

import App from './src/App';
import {name as appName} from './app.json';

const store = configureStore();

const ReduxStore = () => {
  return (
    <Provider store={store}>
        <App />
    </Provider>
  )
}

AppRegistry.registerComponent(appName, () => ReduxStore);
